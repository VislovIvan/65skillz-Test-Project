//
//  Contacts_data.swift
//  vislov_65skillz_test
//
//  Created by Ivan Vislov on 26.09.2021.
//

import SwiftUI
import Foundation

struct Contact: Identifiable {
    let id = UUID()
    let firstName: String
    let lastName: String
    let phoneNumber: String
    let email: String
    let dateOfBirth: String
    let company: String
}

let contacts = [
    Contact(firstName: "Tim", lastName: "Cook", phoneNumber: "+ 7 (911) 111-1111", email: "tcook@apple.com", dateOfBirth: "01.10.1960", company: "Apple"),
    Contact(firstName: "Jony", lastName: "Ive", phoneNumber: "+ 7 (922) 222-2222", email: "jive@lovefrom.com", dateOfBirth: "27.02.1967", company: "LoveFrom"),
    Contact(firstName: "Sundar", lastName: "Pichai", phoneNumber: "+ 7 (933) 333-3333", email: "sundar@google.com", dateOfBirth: "12.07.1972", company: "Apple"),
    Contact(firstName: "Pavel", lastName: "Durov", phoneNumber: "+ 7 (944) 444-4444", email: "durov@telegram.com", dateOfBirth: "12.07.1972", company: "Apple")
]

