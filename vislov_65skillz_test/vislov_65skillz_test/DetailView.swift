//
//  DetailView.swift
//  vislov_65skillz_test
//
//  Created by Ivan Vislov on 26.09.2021.
//

import SwiftUI

struct DetailView: View {
    
    let contact: Contact
    
    var body: some View {
        VStack {
            HStack {
                Text(contact.firstName)
                    .font(.title)
                    .fontWeight(.medium)
                Text(contact.lastName)
                    .font(.title)
                    .fontWeight(.medium)
            }
            Form {
                HStack {
                    Text("Date of Birth")
                    Spacer()
                    Text(contact.dateOfBirth)
                        .foregroundColor(.gray)
                        .font(.callout)
                    
                }
                HStack {
                    Text("Phone")
                    Spacer()
                    Text(contact.phoneNumber)
                        .foregroundColor(.gray)
                        .font(.callout)
                    
                }
                HStack {
                    Text("Email")
                    Spacer()
                    Text(contact.email)
                        .foregroundColor(.gray)
                        .font(.callout)
                    
                }
                HStack {
                    Text("Company")
                    Spacer()
                    Text(contact.company)
                        .foregroundColor(.gray)
                        .font(.callout)
                    
                }
            }
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(contact: contacts[0])
    }
}
